class Amphibian  < Animal 

attr_accessor :num_legs, :fur, :eggs, :gills,:warm_blooded

def initialize(num_legs, fur, eggs, gills, warm_blooded)
  @num_legs = 4
  @fur = false
  @eggs = true
  @gills = true
  @warm_blooded = false
  end

end
