require_relative 'support'

class Mammal < Animal
include Flight
attr_accessor :num_legs, :fur, :eggs, :gills,:warm_blooded

  def initialize(num_legs, fur, eggs, gills, warm_blooded)
    @num_legs = 4
    @fur = true
    @eggs = false
    @gills = false
    @warm_blooded = true
  end


end
