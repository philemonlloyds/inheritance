class Primate < Animal

attr_accessor :num_legs, :fur, :eggs, :gills,:warm_blooded

  def initialize(num_legs, fur, eggs, gills, warm_blooded)
    @num_legs = 2
    @fur = "yes"
    @eggs = "no"
    @gills = "no"
    @warm_blooded = "yes"
  end


end


