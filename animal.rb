class Animal

attr_accessor :num_legs, :fur, :eggs, :gills,:warm_blooded

  def initialize(num_legs, fur, eggs, gills, warm_blooded)
    @num_legs = num_legs
    @fur = fur
    @eggs = eggs
    @gills = gills
    @warm_blooded = warm_blooded
  end


 def warm_blooded?
  warm_blooded == true ? true : false
 end

end

